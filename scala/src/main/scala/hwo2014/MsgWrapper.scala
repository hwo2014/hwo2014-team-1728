package hwo2014

import org.json4s._


/**
 * Messages management
 */
case class MsgWrapper(msgType: String, data: JValue)

case class MsgWrapperFull(msgType: String, data: JValue, gameId: Option[String], gameTick: Option[Int]) {
  def getMsgWrapper: MsgWrapper = {
    MsgWrapper(msgType, data)
  }
}

object MsgWrapper {
  implicit val formats = new DefaultFormats {}

  def apply(msgType: String, data: Any): MsgWrapper = {
    MsgWrapper(msgType, Extraction.decompose(data))
  }
}

object MsgWrapperFull {
  implicit val formats = new DefaultFormats {}

  def apply(msgType: String, data: Any, gameId: Option[String], gameTick: Option[Int]): MsgWrapperFull = {
    MsgWrapperFull(msgType, Extraction.decompose(data), gameId, gameTick)
  }
}
