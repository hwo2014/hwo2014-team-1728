package hwo2014

import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import scala.annotation.tailrec

case class Track(id: String, name: String, pieces: List[Piece], lanes: List[Lane], startingPoint: StartingPoint) {
  val switches: List[Switch] = getSwitches
  var lastSentSwitch: Int = 0
  setPiecesIndex()
  initializeCurves()

  def setPiecesIndex(): Unit = {
    var i = 0
    pieces.foreach(piece => {
      piece.index = i
      i += 1
    })
  }

  def getSwitches: List[Switch] = {
    var switchList = ListBuffer[Switch]()
    val switchesVar: mutable.Queue[Int] = mutable.Queue[Int]()

    // Getting list of switches in the track
    var i = 0
    pieces.foreach(piece => {
      if (piece.isSwitch)
        switchesVar.enqueue(i)
      i += 1
    })

    val firstAndLastSwitch = switchesVar.front

    while (!switchesVar.isEmpty) {

      // index of the next switch
      val switchIndex: Int = switchesVar.dequeue
      // index of the following switch
      val nextSwitchIndex: Int =
        if (!switchesVar.isEmpty)
          switchesVar.front
        else
          firstAndLastSwitch


      val map: List[(Int, Double)] = lanes.map(lane => {
        (lane.index, getDistanceBetweenPiecesOnLane(getNextPieceIndex(switchIndex), nextSwitchIndex, lane))
      })

      val bestDistance = map.reduceLeft((elementA, elementB) => {
        if (elementA._2 < elementB._2)
          elementA
        else {
          if (elementA._2 == elementB._2) {
            (-1, elementA._2)
          }
          else {
            elementB
          }
        }
      })


      // Determine the best lane
      val bestLane: Option[Lane] =
        if (bestDistance._1 == -1)
          None
        else
          Some(lanes(bestDistance._1))

      println("[SWITCH] Searching for best path between " + switchIndex + " & " + nextSwitchIndex)
      if (bestLane.isDefined) {
        println("Found best lane" + bestLane.toString)
        switchList += Switch(switchIndex, bestLane)
      }
      else {
        println("No best lane found between")
        switchList += Switch(switchIndex, None)
      }
    }

    switchList.toList
  }

  def getDistanceBetweenPiecesOnLane(startIndex: Int, endIndex: Int, lane: Lane): Double = {
    if (startIndex <= endIndex)
      pieces.dropRight(pieces.length - endIndex).drop(startIndex).foldLeft(0.0)((b, a) => b + getLengthOfPiece(a, lane))
    else
      pieces.filter(p => p.index < endIndex || p.index > startIndex).foldLeft(0.0)((b, a) => b + getLengthOfPiece(a, lane))
  }

  def switchLane(carInfos: List[CarInfo], me: CarId): String = {

    // Our position
    val carPosition = carInfos.find(carPosition => carPosition.id.name == me.name)

    if (carPosition.isDefined) {
      val myPosition = carPosition.get
      val nextSwitch = switches.find(switch => switch.index - 1 == myPosition.index)
      val currentIndex = myPosition.index

      if (nextSwitch.isDefined && currentIndex != lastSentSwitch) {
        lastSentSwitch = currentIndex

        // Get a switch with a preferred lane
        val switch = getValidSwitchForIndex(nextSwitch.get)

        val bestLane = getBestLaneFromOthers(carInfos.filter(cp => cp.id.name != me.name), myPosition, switch.lane.get)

        if (bestLane != myPosition.laneIndex) {
          if (bestLane < myPosition.laneIndex) {
            // Best lane is on the left
            "Left"
          }
          else {
            // Best lane is on the right
            "Right"
          }
        }
        else
          null
      }
      else
        null
    }
    else
    // No switch to send for now
      null
  }

  def getBestLaneFromOthers(otherCarInfos: List[CarInfo], myInfos: CarInfo, targetLane: Lane): Int = {
    // cars in front of us or in the next piece
    val carsInFrontOfUs = otherCarInfos.filter(ci => ci.index == myInfos.index && ci.inPieceDistance > myInfos.inPieceDistance ||
      (ci.index == getNextPieceIndex(myInfos.index) && ci.inPieceDistance < getLengthOfPiece(pieces(ci.index), lanes(ci.laneIndex)) / 4.0))

    // slow cars in front of us
    val slowCarsInFrontOfUs = carsInFrontOfUs.filter(ci => ci.speed < myInfos.speed)

    // number of slow cars in front of us in each lane
    val laneList = lanes.map(lane => {
      (lane.index, slowCarsInFrontOfUs.count(ci => ci.laneIndex == lane.index))
    })

    val bestLane = laneList.reduceLeft((laneA, laneB) => if (laneA._2 < laneB._2) laneA else laneB)
    val tgtLane = laneList.find(lane => lane._1 == targetLane.index)

    if (tgtLane.get._2 <= bestLane._2)
      tgtLane.get._1
    else
      bestLane._1
  }

  // Returns the next switch that has a best lane.
  @tailrec final def getValidSwitchForIndex(switch: Switch): Switch = {
    if (!switch.lane.isDefined) {
      val switchIndex = switches indexOf switch
      if (switchIndex + 1 >= switches.size) {
        getValidSwitchForIndex(switches(0))
      }
      else
        getValidSwitchForIndex(switches(switchIndex + 1))
    }
    else {
      switch
    }
  }

  def getPreviousPieceIndex(index: Int) = {
    // % -> security last piece
    (this.pieces.length + index - 1) % this.pieces.length
  }

  def getNextPieceIndex(index: Int) = {
    // % -> security last piece
    (index + 1) % this.pieces.length
  }

  lazy val getCurves: Seq[Curve] = {

    val mapOfPieces = this.pieces.foldLeft((Map[(Double, Int), Option[Seq[(Int, Seq[Piece])]]](), 0))((res, piece) => {
      if (piece.isCurve) {
        // si on a déjà des pièces enregistrées pour cet angle
        if (res._1.get((piece.angle.get, piece.radius.get)).isDefined) {

          // on récupère les pièces associées à cet angle et à ce radius
          val angleCurve: Seq[(Int, Seq[Piece])] = res._1.get((piece.angle.get, piece.radius.get)).get.get

          // la map sans cet angle
          val mapWithoutAngle = res._1.filterNot(p => p._1 ==(piece.angle.get, piece.radius.get))

          // le dernier virage associé à cet angle
          val lastAngleCurve = angleCurve.last

          // si la dernière pièce du dernier virage avec le même angle précède notre pièce
          // alors on est dans le meme virage
          // TODO : gérer le cas ou la dernière pièce est dans le même virage que la 1ère pièce
          if (lastAngleCurve._2.last.index + 1 == piece.index) {
            (mapWithoutAngle + ((piece.angle.get, piece.radius.get) -> Some(angleCurve.dropRight(1) :+(lastAngleCurve._1, lastAngleCurve._2 :+ piece))), res._2)
          } else {
            (mapWithoutAngle + ((piece.angle.get, piece.radius.get) -> Some(angleCurve :+(res._2 + 1, Seq(piece)))), res._2 + 1)
          }
        } else {
          (res._1 + ((piece.angle.get, piece.radius.get) -> Some(Seq[(Int, Seq[Piece])]((res._2 + 1, Seq(piece))))), res._2 + 1)
        }
      } else {
        (res._1 + ((0.0, 0) -> None), res._2)
      }
    })

    // debug
    println("nombre de virages: " + mapOfPieces._2)
    val curves = new Array[Curve](mapOfPieces._2)
    mapOfPieces._1.foreach(t => {
      if (t._2.isDefined) {
        val currentCurves: Seq[(Int, Seq[Piece])] = t._2.get
        currentCurves.foreach(curve => {
          val finalIndex = if (this.pieces((curve._2.last.index + 1) % this.pieces.size).isStraight) (curve._2.last.index + 1) % this.pieces.size else curve._2.last.index
          curves(curve._1 - 1) = new Curve(curve._2.head.index, finalIndex, curve._2.head.angle.get, curve._2.head.radius.get)
        })
      }
    })

    def areSameSign(a: Double, b: Double) = {
      if (a < 0 && b < 0)
        true
      else if (a > 0 && b > 0)
        true
      else
        false
    }

    curves.toSeq.foldLeft(Seq[Curve]())((res, currentCurve) => {
      if (res.isEmpty)
        res :+ currentCurve
      else if (res.last.endIndex == currentCurve.startIndex - 1 && this.pieces(res.last.endIndex).isCurve && areSameSign(res.last.angle, currentCurve.angle) && Math.abs(res.last.angle) > Math.abs(currentCurve.angle)) {
        res.dropRight(1) :+ res.last.copy(endIndex = currentCurve.endIndex)
      } else {
        res :+ currentCurve
      }
    })
  }

  def initializeCurves(): Unit = {
    getCurves.foreach(curve => {
      val speedMap = lanes.map(lane => {
        lane.index -> getSpeedLimitFromTurn(curve.angle, curve.radius, lane)
      }).toMap

      val angleMap = lanes.map(lane => {
        lane.index -> 0.0
      }).toMap

      curve.speedLimit ++= speedMap
      curve.worstAngle ++= angleMap
    })
  }

  def getCurveFromIndex(index: Int): Curve = {
    val curve = getCurves.find(c => index >= c.startIndex && index <= c.endIndex)
    if (curve.isDefined)
      curve.get
    else
      null
  }

  /**
   * Get length of piece
   * @param piece piece
   * @param currentLane lane used
   * @return length of piece
   */
  def getLengthOfPiece(piece: Piece, currentLane: Lane): Double = {
    def getLengthOfTurn(angle: Double, radius: Int, lane: Lane) = {
      val rayon =
        if (angle > 0.0)
          radius - lane.distanceFromCenter
        else
          radius + lane.distanceFromCenter

      2 * scala.math.Pi * rayon * (Math.abs(angle) / 360.0)
    }

    if (piece.length.isDefined)
      piece.length.get
    else
      getLengthOfTurn(piece.angle.get, piece.radius.get, currentLane)
  }

  lazy val getTurboIndex: Int = {
    val largeCurves = getCurves.filter(c => Math.abs(c.angle) > 22.5)

    val distances = largeCurves.map(c => {
      val next = largeCurves((largeCurves.indexOf(c) + 1) % largeCurves.size)
      val switch = switches.find(s => s.index >= c.endIndex && s.index <= next.startIndex)
      val lane =
        if (switch.isDefined) {
          val goodSwitch = getValidSwitchForIndex(switch.get)
          goodSwitch.bestLane.get
        }
        else
          lanes(0)

      (c, next) -> getDistanceBetweenPiecesOnLane(c.endIndex, next.endIndex, lane)
    })

    distances.foreach(d => println("From Curve: " + d._1._1.endIndex + " to " + d._1._2.startIndex + "\ndistance: " + d._2))

    val bestSegment = distances.reduceLeft((distanceA, distanceB) => if (distanceA._2 >= distanceB._2) distanceA else distanceB)

    if (pieces(bestSegment._1._1.endIndex).isStraight)
      bestSegment._1._1.endIndex
    else
      getNextPieceIndex(bestSegment._1._1.endIndex)
  }

  def getDistanceToNextCurveFromCarPosition(carPosition: CarPosition): Double = {
    val currentLane = lanes(carPosition.piecePosition.lane.endLaneIndex)

    val baseDistance = getLengthOfPiece(pieces(carPosition.piecePosition.pieceIndex), currentLane) -
      carPosition.piecePosition.inPieceDistance

    // Get distance to break and speed at distance
    getDistanceToNextCurve(carPosition.piecePosition.pieceIndex, currentLane, baseDistance)
  }

  def getDistanceToNextCurve(fromIndex: Int, lane: Lane, baseDistance: Double): Double = {
    val nextCurve = getNextCurveFromIndex(fromIndex)

    val origin =
      if (baseDistance == 0.0)
        fromIndex
      else
        getNextPieceIndex(fromIndex)

    val distance = getDistanceBetweenPiecesOnLane(origin, nextCurve.startIndex, lane)

    baseDistance + distance
  }

  def getNextCurveFromIndex(index: Int): Curve = {
    val nextCurve = getCurves.filter(curve => curve.startIndex > index)

    if (!nextCurve.isEmpty) {
      nextCurve.head
    }
    else {
      getCurves(0)
    }
  }

  def updateCurveSpeeds(lap: LapFinished): Unit = {
    val improveSpeed =
      if (lap.ranking.fastestLap != 1) {
        // We are not the fastest one
        if (lap.ranking.overall == 1) {
          // But we are the first one
          false
        }
        else {
          // Not the fastest and not first --> improve
          true
        }
      }
      else
      // We're the fastest!
        false

    if (improveSpeed) {
      println("[UPDATE SPEED] We will improve")
      getCurves.foreach(curve => {
        curve.worstAngle.foreach(angle => {
          if (!curve.maxImprovementReached) {

            // If there is a max speed reached in this turn, meaning we passed it without crashing
            if (curve.maxSpeedReached.get(angle._1).isDefined) {

              // If the max speed reached is lower than the speed limit
              // lower the speed limit to the speed actually reached in the turn
              if (curve.maxSpeedReached.get(angle._1).get < curve.speedLimit.get(angle._1).get) {
                curve.speedLimit.put(angle._1, curve.maxSpeedReached.get(angle._1).get)

              }
              else {
                // Store oldSpeed limit
                val oldSpeed = curve.speedLimit.get(angle._1).get

                // New speed will be...
                val newSpeed =
                  angle._2 match {
                    case 0.0 => oldSpeed
                    case x: Double if x < 10.0 =>
                      curve.safeSpeeds.put(angle._1, oldSpeed)
                      oldSpeed + 0.8
                    case x: Double if x < 30.0 =>
                      curve.safeSpeeds.put(angle._1, oldSpeed)
                      oldSpeed + 0.5
                    case x: Double if x < 40.0 =>
                      curve.safeSpeeds.put(angle._1, oldSpeed)
                      oldSpeed + 0.4
                    case x: Double if x < 45.0 =>
                      curve.safeSpeeds.put(angle._1, oldSpeed)
                      oldSpeed + 0.15
                    case x: Double if x < 50.0 =>
                      curve.safeSpeeds.put(angle._1, oldSpeed)
                      oldSpeed + 0.05
                    case x: Double if x > 55.0 =>
                      // Decrement speed! we don't want to crash and we're going to accelerate globally
                      // invalidate old worst angle && invalidate old max speed
                      curve.safeSpeeds.put(angle._1, oldSpeed - 0.2)
                      curve.maxSpeedReached.remove(angle._1)
                      curve.worstAngle.put(angle._1, 0.0)
                      oldSpeed - 0.2
                    case _ =>
                      oldSpeed
                  }

                curve.speedLimit.put(angle._1, if (newSpeed > 0.0) newSpeed else 1.0)
              }
            }
          }
        })
      })
    }
    else
      println("[UPDATE SPEED] We will not improve")
  }

  def countPiecesBetween(from: Int, to: Int): Int = {
    if (from <= to)
      to - from + 1
    else
      pieces.size - from + to
  }

  def getNextCurve(curve: Curve): Curve = {
    val curveIndex = getCurves.indexOf(curve)
    getCurves((curveIndex + 1) % getCurves.size)
  }

  def getSameCurves(curve: Curve): Seq[Curve] = {
    getCurves.filter(c => Math.abs(c.angle) == Math.abs(curve.angle) && c.radius == curve.radius && c.count == curve.count)
  }

  override def toString: String = {
    s"""
      Track: $id /  $name \n
      Size:  ${pieces.length} \n
      Lanes: ${lanes.length} \n
      Starting Point: $startingPoint \n
      ${pieces.toSeq.foldLeft("")((b, a) => b + s"""Piece ${a.index}: ${a.toString}\n""")}
     """
  }

  /**
   * Get radius from angle, radius and distanceFromCenter
   * @param angle angle
   * @param radius radius
   * @param distanceFromCenter distance from center
   * @return
   */
  def getFullRadius(angle: Double, radius: Int, distanceFromCenter: Int): Int = {
    if (angle > 0.0)
      radius + (-distanceFromCenter)
    else
      radius + distanceFromCenter
  }

  /**
   * Generate speed limit from angle radius and lane of turn
   * @param angle angle
   * @param radius radius
   * @param lane lane
   * @return
   */
  def getSpeedLimitFromTurn(angle: Double, radius: Int, lane: Lane): Double = {
    val rayon = getFullRadius(angle, radius, lane.distanceFromCenter)

    Math.abs(angle) match {
      case 45.0 =>
        rayon match {
          case 30 =>
            4.0
          case 40 =>
            4.6
          case 60 =>
            5.6
          case 90 =>
            6.4
          case 110 =>
            6.9
          case x =>
            6.6
        }
      case 22.5 =>
        rayon match {
          case x =>
            15.0
        }
      case x =>
        6.6
    }
  }
}

/**
 * Lane
 * @param distanceFromCenter represents the distance of the lane from the center of the piece (i.e. -20 or 0 or 20)
 * @param index represents the index of the lane (i.e. 0, 1 or 2)
 */
case class Lane(distanceFromCenter: Int, index: Int) {
  override def toString: String = s"""Lane{distanceFromCenter=$distanceFromCenter, index=$index}"""
}

/**
 * Starting position
 * @param position position of the car
 * @param angle angle of starting position (TODO: understand what this means)
 */
case class StartingPoint(position: Position, angle: Double)

/**
 * Position
 * @param x coordinate x
 * @param y coordinate y
 */
case class Position(x: Double, y: Double) {
  override def toString: String = s"""Position{x=$x, y= $y }"""
}

case class Switch(startIndex: Int, bestLane: Option[Lane]) {
  val index = startIndex
  val lane = bestLane
}

case class Curve(startIndex: Int, endIndex: Int, angle: Double, radius: Int) {
  var worstAngle: mutable.Map[Int, Double] = mutable.Map[Int, Double]()
  var speedLimit: mutable.Map[Int, Double] = mutable.Map[Int, Double]()
  val count = endIndex - startIndex + 1
  var safeSpeeds: mutable.Map[Int, Double] = mutable.Map[Int, Double]()
  var maxSpeedReached: mutable.Map[Int, Double] = mutable.Map[Int, Double]()
  var maxImprovementReached = false

  def updateSpeedLimit(speed: Double, track: Track): Unit = {
    speedLimit.foreach(s => {
      if (worstAngle.get(s._1).get == 0.0) {
        val distanceFromCenter = track.lanes(s._1).distanceFromCenter

        val coefficient =
          if (angle > 0.0)
            -distanceFromCenter / 100.0
          else
            distanceFromCenter / 100.0

        speedLimit.put(s._1, speed + coefficient * 2)
      }

    })
  }

  override def toString: String = {
    s"""
      startIndex: $startIndex, endIndex:  $endIndex, angle: $angle, radius: $radius
      maxSpeedReached:
      ${maxSpeedReached.toSeq.foldLeft("")((b, a) => b + s"""Lane ${a._1}: ${a._2}, """)}
      speedLimit:
      ${speedLimit.toSeq.foldLeft("")((b, a) => b + s"""Lane ${a._1}: ${a._2}, """)}
      worstAngle:
      ${worstAngle.toSeq.foldLeft("")((b, a) => b + s"""Lane ${a._1}: ${a._2}, """)}
    """
  }
}

