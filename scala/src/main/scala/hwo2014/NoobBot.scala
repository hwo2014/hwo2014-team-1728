package hwo2014

import org.json4s._
import org.json4s.DefaultFormats
import java.net.Socket
import java.io.{BufferedReader, InputStreamReader, OutputStreamWriter, PrintWriter}
import org.json4s.native.Serialization
import scala.annotation.tailrec
import scala.Some

object NoobBot extends App {
  args.toList match {
    case hostName :: port :: botName :: botKey :: _ =>
      new NoobBot(hostName, Integer.parseInt(port), botName, botKey)
    case _ => println("args missing")
  }
}

class NoobBot(host: String, port: Int, botName: String, botKey: String) {
  implicit val formats = new DefaultFormats {}
  val socket = new Socket(host, port)
  val writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream, "UTF8"))
  val reader = new BufferedReader(new InputStreamReader(socket.getInputStream, "UTF8"))

  // Custom vars
  var race: Option[Race] = None
  var oldCarPosition: Option[CarPosition] = None
  var speed: Double = Double.MinValue
  var currentSpeedLimit: Double = 0.0
  var turboAvailable = false
  var quickRace = false
  var debug = true
  var turboIsOn = false
  var me: CarId = null
  var isCrashed = false
  // use for velocity coefficient
  var raceStarted = false
  var velocityCoefficient = 1.0
  var oldCarPositions: Option[Map[String, CarPosition]] = None
  var carInfos: List[CarInfo] = null
  var currentLap: Int = 0

  val botId = BotId(botName, botKey)
  // To join other races, uncomment
  //send(MsgWrapper("joinRace", JoinRacePasswd(botId, 1, "suzuka", "cjgzafu")))

  // master should always have this implemented
  send(MsgWrapper("join", botId))
  println("[JOIN] Request sent")
  play()

  /**
   * Get throttle from car position
   * @param carPosition last car position received
   * @return throttle
   */
  private def getThrottle(carPosition: CarPosition): Double = {
    val track = race.get.track
    val index = carPosition.piecePosition.pieceIndex
    val currentLane = track.lanes(carPosition.piecePosition.lane.endLaneIndex)

    val currentPiece = race.get.track.pieces(index)
    speed = calculateSpeed(race.get.track, oldCarPosition.getOrElse(carPosition), carPosition)

    oldCarPosition = Option.apply(carPosition)

    if (!raceStarted && speed > 0.0) {
      velocityCoefficient = speed / 10
      println("coefficient " + velocityCoefficient)
      raceStarted = true
    }

    val curve = track.getCurveFromIndex(index)


    if (curve != null) {
      // Updating worst angle
      if (curve.worstAngle.get(currentLane.index).get < Math.abs(carPosition.angle))
        curve.worstAngle.put(currentLane.index, Math.abs(carPosition.angle))

      // Update max speed if we are not currently using turbo
      // We don't update speed when out of the turn
      if (!turboIsOn && currentPiece.isCurve) {
        // Update maxSpeed reached in turn
        if (!curve.maxSpeedReached.get(currentLane.index).isDefined)
        // No previous speed has been set
          curve.maxSpeedReached.put(currentLane.index, speed)
        else if (curve.maxSpeedReached.get(currentLane.index).get < speed)
        // Update previous speed
          curve.maxSpeedReached.put(currentLane.index, speed)
      }
    }



    val throttle = if (!currentPiece.isCurve)
      getSpeedLimitFromBrakeDistance(carPosition)
    else {
      val brake = getSpeedLimitFromBrakeDistance(carPosition)

      currentSpeedLimit = curve.speedLimit.get(currentLane.index).get

      if ((speed > currentSpeedLimit) || brake == 0.0) {
        0.0
      } else {
        if (Math.abs((currentSpeedLimit - speed) / currentSpeedLimit) > 0.0025 && Math.abs((currentSpeedLimit - speed) / currentSpeedLimit) < 0.025) {
          //println(s"""moyenne ${((currentSpeedLimit - (speed-currentSpeedLimit)*(1.0 - Math.abs(speed-currentSpeedLimit))*velocityCoefficient)/10)}""")
          Math.min(1.0, (currentSpeedLimit - (speed - currentSpeedLimit) * (1.0 - Math.abs(speed - currentSpeedLimit))) / 10)
        } else {
          if (Math.abs((currentSpeedLimit - speed) / currentSpeedLimit) < 0.0025) {
            //print("last else if ")
            Math.min(1.0, currentSpeedLimit / 10)
          } else
            1.0
        }
      }
    }
    //println(s"""curve : ${currentPiece.isCurve}""")
    //println(s"""index: $index, speed: $speed, speedLimit: $currentSpeedLimit => throttle: $throttle""")
    throttle
  }


  def getSpeedLimitFromBrakeDistance(carPosition: CarPosition): Double = {
    val track = race.get.track
    val currentLane = track.lanes(carPosition.piecePosition.lane.startLaneIndex)

    def getNumberOfTicksFromDistance(distance: Double): Double = {
      distance / speed
    }

    def getBrakeDistance(numberOfTicks: Double): Double = {
      // Distance is in unit
      // We run at speed unit per ms
      // ticks are every (1/60)s.
      speed * scala.math.exp(-velocityCoefficient * numberOfTicks)
    }

    @tailrec def getBraking(fromIndex: Int, nextIndex: Int, nextTurn: Boolean, fromDistance: Double): Double = {
      // Get speed limit in next curve
      val nextCurve = track.getNextCurveFromIndex(nextIndex)
      //println("Next curve:" + nextCurve.toString)

      val switch = track.switches.find(s => s.index >= nextIndex && s.index <= nextCurve.startIndex)

      val lane =
        if (switch.isDefined) {
          val goodSwitch = track.getValidSwitchForIndex(switch.get)
          goodSwitch.bestLane.get
        }
        else
          currentLane

      val speedLimitInNextCurve = nextCurve.speedLimit.get(lane.index).get

      // Get distance to break and speed at distance
      val distance =
        if (!nextTurn)
          track.getDistanceToNextCurve(nextIndex, currentLane, fromDistance)
        else
          fromDistance + track.getDistanceToNextCurve(nextIndex, currentLane, 0.0)

      //println("Distance to next curve: " + distance)
      val speedBrakingNow = getBrakeDistance(getNumberOfTicksFromDistance(distance))

      //println("[GETBRAKING] Getting braking between " + fromIndex + " & " + nextCurve.startIndex + ": fromDistance=" + fromDistance + "  distance=" + distance + ", speedBrakingNow= " + speedBrakingNow + ", speedLimit= " + speedLimitInNextCurve)
      if (speedBrakingNow >= speedLimitInNextCurve - 0.1)
        0.0
      else
      // If braking now we will run at less than 1.0 on given curve OR
      // if we already checked half of the track ... leave the recursion
      if (speedBrakingNow < 2.0 || track.countPiecesBetween(fromIndex, nextCurve.endIndex) > track.pieces.size / 2)
        1.0
      else
        getBraking(fromIndex, nextCurve.endIndex, nextTurn = true, distance)
    }

    val baseDistance = track.getLengthOfPiece(track.pieces(carPosition.piecePosition.pieceIndex), currentLane) -
      carPosition.piecePosition.inPieceDistance

    getBraking(carPosition.piecePosition.pieceIndex, carPosition.piecePosition.pieceIndex, nextTurn = false, baseDistance)
  }

  /**
   * Get Speed
   * @param track race track
   * @param oldCarPosition car position at previous game tick
   * @param newCarPosition car position at current game tick
   * @return last speed if new speed is unknown
   */
  def calculateSpeed(track: Track, oldCarPosition: CarPosition, newCarPosition: CarPosition): Double = {

    val oldPieceIndex = oldCarPosition.piecePosition.pieceIndex
    val newPieceIndex = newCarPosition.piecePosition.pieceIndex

    // Entering new piece
    if (oldPieceIndex != newPieceIndex) {
      if (oldCarPosition.piecePosition.lane.startLaneIndex != oldCarPosition.piecePosition.lane.endLaneIndex)
        return speed

      val oldLane = track.lanes(oldCarPosition.piecePosition.lane.endLaneIndex)
      val distanceBefore =
        track.getLengthOfPiece(track.pieces(oldPieceIndex), oldLane) -
          oldCarPosition.piecePosition.inPieceDistance

      // Speed is in 'unit of distance'/tick
      newCarPosition.piecePosition.inPieceDistance + distanceBefore

    }
    // Same piece
    else {
      // Speed is in 'unit of distance'/tick
      newCarPosition.piecePosition.inPieceDistance - oldCarPosition.piecePosition.inPieceDistance
    }
  }


  /**
   * Process crash
   * @param data data received
   * @param gameId gameId
   * @param gameTick gameTick
   */
  def crashReceived(data: JValue, gameId: String, gameTick: Int) {
    val track = race.get.track
    val index = oldCarPosition.get.piecePosition.pieceIndex
    val currentPiece = track.pieces(index)

    def learnFromCrash(curve: Curve, laneIndex: Int, newSpeed: Double): Unit = {
      println("[CRASH] old turn -> " + curve.toString)
      // Reset worst angle
      curve.worstAngle.put(laneIndex, 0.0)

      // get similar curves
      val sameCurves = track.getSameCurves(curve)
      sameCurves.foreach(c =>
        // On similar turns that have not been passed yet and
        if ((c.worstAngle.get(laneIndex).get == 0.0 && currentLap == 0) || c.startIndex == curve.startIndex) {
          // If there was no safeSpeed defined
          if (!c.safeSpeeds.get(laneIndex).isDefined)
            c.updateSpeedLimit(newSpeed, track)
          else {
            // If there was a safeSpeed defined
            val safeSpeed = c.safeSpeeds.get(laneIndex).get

            // If we crashed using the "safeSpeed" as a limit, invalidate this safeSpeed
            if (safeSpeed == c.speedLimit.get(laneIndex).get || c.maxSpeedReached.get(laneIndex).get > c.speedLimit.get(laneIndex).get) {
              println("[SAFESPEED][ERROR] Oops, safe speed wasn't safe after all...")
              c.safeSpeeds.remove(laneIndex)
              c.maxImprovementReached = false
              c.updateSpeedLimit(newSpeed, track)
            }
            else {
              println("[SAFESPEED][REACHED] We will now use safe speed on " + c.toString)
              // Use safe speed as speed limit
              c.updateSpeedLimit(c.safeSpeeds.get(laneIndex).get, track)
              c.maxImprovementReached = true
            }
          }
        }
      )

      // Reset max speed reached
      curve.maxSpeedReached.remove(laneIndex)

      println("[CRASH] Updated at most " + sameCurves.size + " curves to " + newSpeed)
      println("[CRASH] Updated turn -> " + curve.toString)
    }

    // Get car which crashed
    val car = data.extract[CarId]

    // If it's us...
    if (car.name == me.name) {
      turboAvailable = false
      isCrashed = true

      println("[CRASH] at index: " + index)
      println("[CRASH] at speed: " + speed)
      println("[CRASH] at speed limit: " + currentSpeedLimit)

      val curve = track.getCurveFromIndex(currentPiece.index)
      if (curve != null) {
        val laneIndex = oldCarPosition.get.piecePosition.lane.endLaneIndex
        val oldSpeedLimit = curve.speedLimit.get(laneIndex).get

        val newValue =
          if (oldSpeedLimit < speed)
            oldSpeedLimit - 1.0
          else
            speed - 1.0

        // Learn from crash
        learnFromCrash(curve, laneIndex, newValue)
      }
    }
    else {
      println("[CRASH] " + car.name + " crashed ... LOL")
      val crashedCar = carInfos.find(ci => ci.id.name == car.name)

      // Crashed car has a last position
      if (crashedCar.isDefined) {

        // Piece on which he crashed
        val currentPiece = track.pieces(crashedCar.get.index)

        // If he crashed in a turn

        val curve = track.getCurveFromIndex(currentPiece.index)
        if (curve != null) {
          val laneIndex = crashedCar.get.laneIndex
          val oldSpeedLimit = curve.speedLimit.get(laneIndex).get

          // If he crashed going slower than we would have, else he's just insane so don't do anything
          if (oldSpeedLimit > crashedCar.get.speed) {
            learnFromCrash(curve, laneIndex, crashedCar.get.speed - 0.1)
            println("Hey! I learnt something from someone's crash :D")
          }
        }
      }
    }
    // for getThrottle
    speed = Double.MinValue
  }

  /**
   * Process carPosition
   * @param data data received
   * @param gameId gameId
   * @param gameTick gameTick
   */
  def carPositionReceived(data: JValue, gameId: Option[String], gameTick: Option[Int]) {
    try {
      val carPositions: List[CarPosition] = data.extract[List[CarPosition]]

      /**
       * Process others positions
       */
      val newCarPositions = carPositions.map(cp => {
        cp.id.name -> cp
      }).toMap

      carInfos = newCarPositions.map(cp => {
        if (oldCarPositions.isDefined)
          new CarInfo(cp._2.id, calculateSpeed(race.get.track, oldCarPositions.get(cp._1), cp._2), cp._2.piecePosition.pieceIndex, cp._2.piecePosition.inPieceDistance, cp._2.piecePosition.lane.endLaneIndex)
        else
          new CarInfo(cp._2.id, 0.0, cp._2.piecePosition.pieceIndex, cp._2.piecePosition.inPieceDistance, cp._2.piecePosition.lane.endLaneIndex)
      }).toList

      // Store carPositions in oldCarPositions
      oldCarPositions = Some(newCarPositions)

      /**
       * Process our position
       */
      val ourBotLastPosition = carPositions.find(carPosition => carPosition.id.name == me.name)

      if (ourBotLastPosition.isDefined && !isCrashed) {
        val track = race.get.track
        val index = ourBotLastPosition.get.piecePosition.pieceIndex
        val currentLane = track.lanes(ourBotLastPosition.get.piecePosition.lane.endLaneIndex)

        // Get throttle from position
        val newThrottle = getThrottle(ourBotLastPosition.get)

        // Get switch
        val switch = race.get.track.switchLane(carInfos, me)

        val distance = track.getDistanceToNextCurveFromCarPosition(ourBotLastPosition.get)

        // Turbo
        if (index == track.getTurboIndex && turboAvailable) {
          send(MsgWrapperFull("turbo", "EAT DUST!", gameId, gameTick))
          turboAvailable = false
          println("Turbo used with distance : " + distance)
        }
        else {
          // If we have to switch
          if (switch != null) {
            send(MsgWrapperFull("switchLane", switch, gameId, gameTick))
          }
          else {
            send(MsgWrapperFull("throttle", newThrottle, gameId, gameTick))
          }
        }

        // Debug data
        if (gameTick.isDefined && debug) {
          val rayon =
            if (track.pieces(index).isCurve) {
              track.getFullRadius(track.pieces(index).angle.get, track.pieces(index).radius.get, currentLane.distanceFromCenter)
            }
            else 0
          println(index +
            ", " + gameTick.get +
            ", " + speed +
            ", " + currentSpeedLimit +
            ", " + ourBotLastPosition.get.piecePosition.lane.endLaneIndex +
            ", " + rayon +
            ", " + track.pieces(index).angle.getOrElse(0.0) +
            ", " + track.pieces(index).radius.getOrElse(0) +
            ", " + ourBotLastPosition.get.angle +
            ", " + newThrottle +
            ", " + distance
          )
        }
      }
      else if (isCrashed) {
        send(MsgWrapperFull("ping", null, null, gameTick))
      }
    }
    catch {
      case e: Exception =>
        e.printStackTrace()
    }
  }

  /**
   * Process gameEnd
   * @param data data received
   */
  def gameEndReceived(data: JValue) {
    try {
      val gameEnd: GameEnd = data.extract[GameEnd]
      println(gameEnd.toString)
    }
    catch {
      case e: Exception =>
        println("gameEndReceived: " + e.toString)
        e.printStackTrace()
    }
  }

  /**
   * Main method play
   */
  @tailrec private def play() {
    val line = reader.readLine()

    if (line != null) {
      // Try to match a full message.
      val jsonMessage = line
      val message = Serialization.read[MsgWrapperFull](line)
      var processed = true

      message match {

        case MsgWrapperFull("gameStart", _, gameId, gameTick) =>
          println("[GAMESTART]")
          send(MsgWrapperFull("throttle", 1.0, gameId, gameTick))

        case MsgWrapperFull("carPositions", data, gameId, gameTick) =>
          carPositionReceived(data, gameId, gameTick)

        case MsgWrapperFull("turboStart", data, gameId, gameTick) =>
          val car = data.extract[CarId]
          if (car.name == me.name) {
            println("[TURBO] GO!")
            turboIsOn = true
            send(MsgWrapperFull("ping", null, null, gameTick))
          }

        case MsgWrapperFull("turboEnd", data, gameId, gameTick) =>
          val car = data.extract[CarId]
          if (car.name == me.name) {
            println("[TURBO] END!")
            turboIsOn = false
            send(MsgWrapperFull("ping", null, null, gameTick))
          }

        case MsgWrapperFull("crash", data, gameId, gameTick) =>
          crashReceived(data, gameId.get, gameTick.get)

        case MsgWrapperFull("spawn", data, gameId, gameTick) =>
          val carId = data.extract[CarId]
          if (carId.name == me.name) {
            isCrashed = false
            send(MsgWrapperFull("throttle", 1.0, gameId, gameTick))
          }
          println("[SPAWN] " + carId.name)


        case MsgWrapperFull(msgType, data, gameId, gameTick) =>
          processed = false
      }

      // Message did not contain neither gameId nor gameTick, try to match a simple Msg.
      if (!processed) {
        message.getMsgWrapper match {

          case MsgWrapper("yourCar", data) =>
            me = data.extract[CarId]
            println("[YOURCAR] " + me.toString)

          case MsgWrapper("gameInit", data) =>
            println("[GAMEINIT]")
            try {
              if (!race.isDefined) {
                val raceWp: RaceWrapper = data.extract[RaceWrapper]
                race = Some(raceWp.race)
                race.get.track.getCurves
                println(race.get.toString)
                race.get.track.getCurves.foreach(println)
                if (race.get.raceSession.isDefined)
                  quickRace = race.get.raceSession.get.quickRace
                println(race.get.track.toString)
                println("Turbo at index: " + race.get.track.getTurboIndex)
              }
            }
            catch {
              case e: Exception =>
                println("gameInit: " + e.toString)
            }

          case MsgWrapper("gameEnd", data) =>
            println("[GAMEEND]")
            race.get.track.getCurves.foreach(println)
            gameEndReceived(data)

          case MsgWrapper("turboAvailable", data) =>
            println("[TURBO AVAILABLE]")
            if (!isCrashed)
              turboAvailable = true

          case MsgWrapper("lapFinished", data) =>
            val lap: LapFinished = data.extract[LapFinished]
            if (lap.car.name == me.name) {
              race.get.track.updateCurveSpeeds(lap)
              race.get.track.getCurves.foreach(println)
              currentLap = lap.lapTime.lap + 1
            }
            println("[LAPFINISHED] " + lap.toString)

          case MsgWrapper("tournamentEnd", data) =>
            println("[TOURNAMENTEND]")

          case MsgWrapper("join", data) =>
            println("[JOIN] Request received by server.")

          case MsgWrapper("finish", data) =>
            val car = data.extract[CarId]
            if (car.name == me.name)
              debug = false
            println("[FINISH] " + car.toString)


          case MsgWrapper(msgType, data) =>
            println("[UNKNOWN]")
            println("This message was not processed:")
            println("Received: " + msgType)
            println("Message: " + jsonMessage)
        }
      }
      play()
    }

  }

  /**
   * Send basic message
   * @param msg MsgWrapper
   */
  def send(msg: MsgWrapper) {
    writer.println(Serialization.write(msg))
    writer.flush()
  }

  /**
   * Send full message
   * @param msg MsgWrapperFull
   */
  def send(msg: MsgWrapperFull) {
    writer.println(Serialization.write(msg))
    writer.flush()
  }
}

/**
 * BotId
 * @param name name of the bot
 * @param key key of the bot
 */
case class BotId(name: String, key: String)

/**
 * JoinRace
 * @param botId bot id
 * @param carCount nb of car to race against
 * @param trackName track name
 */
case class JoinRace(botId: BotId, carCount: Int, trackName: String)

/**
 * JoinRaceWithPasswd
 * @param botId bot id
 * @param carCount nb of car to race against
 * @param trackName track name
 * @param password password
 */
case class JoinRacePasswd(botId: BotId, carCount: Int, trackName: String, password: String)


