package hwo2014

/**
 *
 * @param length length of the piece (XOR radius & angle)
 * @param switch if there is a switch on the piece
 * @param radius radius of the piece (linked to angle, XOR length)
 * @param angle angle of the piece (linked to radius, XOR length)
 */
case class Piece(length: Option[Double], switch: Option[Boolean], radius: Option[Int], angle: Option[Double]) {
  var index = 0
  def isStraight: Boolean = {
    length.isDefined
  }

  def isCurve: Boolean = {
    radius.isDefined && angle.isDefined
  }

  def isSwitch: Boolean = {
    switch.isDefined
  }

  override def toString: String = {
    "Angle:" + angle.getOrElse(0.0) +
      ", Length:" + length.getOrElse(0.0) +
      ", Radius:" + radius.getOrElse(0.0) +
      ", Switch:" + switch.getOrElse(false)
  }
}
