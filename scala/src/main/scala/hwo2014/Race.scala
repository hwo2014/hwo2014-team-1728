/**
 * hwo2014bot
 * hwo2014
 */

package hwo2014

case class RaceWrapper(race: Race)

case class Race(track: Track, cars: List[Car], raceSession: Option[RaceSession], qualifyingDuration: Option[Qualification]) {
  override def toString: String = {
    var string = ""
    string += track.id + " / " + track.name + "\n"
    if (raceSession.isDefined) {
      string += "Laps: " + raceSession.get.laps + "\n"
      string += "QuickRace: " + raceSession.get.quickRace + "\n"
    }

    string
  }
}

case class Qualification(durationMs: Int)

/**
 * Race Session
 * @param laps number of laps in session
 * @param maxLapTimeMs maximum time duration of a lap in ms.
 * @param quickRace true if it is a quick race (a test basically) or false if it is qualifying rounds (or tournament?)
 */
case class RaceSession(laps: Int, maxLapTimeMs: Int, quickRace: Boolean)

case class GameEnd(results: List[Result], bestLaps: List[BestResult]) {
  override def toString: String = {
    var string: String = ""

    bestLaps.foreach(bestLap => {
      string += bestLap.car.toString
      if (bestLap.result.isDefined)
        string += bestLap.result.get.toString
    })

    string
  }
}

case class Result(car: CarId, result: Option[RaceTime], error: Option[Error])

case class BestResult(car: CarId, result: Option[LapTime], error: Option[Error])

case class Error(reason: String) {
  override def toString: String = {
    "Reason: " + reason + "\n"
  }
}


case class RaceTime(laps: Int, ticks: Int, millis: Int)

case class LapTime(lap: Int, ticks: Int, millis: Int) {
  override def toString: String = {
    var string = ""
    string += "LapId: " + lap + "\n"
    string += "Time: " + millis / 1000.0 + "s.\n"
    string += "Ticks: " + ticks + "\n"
    string
  }
}

case class LapFinishedWrapper(lapFinished: LapFinished)

case class LapFinished(car: CarId, lapTime: LapTime, raceTime: RaceTime, ranking: Ranking) {
  override def toString: String = {
    s"""${car.toString} laptime : ${lapTime.toString} ranking : $ranking\n"""
  }
}

case class Ranking(overall: Int, fastestLap: Int)

case class TurboAvailable(turboDurationMilliseconds: Double, turboDurationTicks: Int, turboFactor: Double) {
  override def toString: String = {
    "TurboAvailable: turboDuration:" + turboDurationMilliseconds + "ms / ticks:" +
      turboDurationTicks + " / factor:" + turboFactor
  }
}