/**
 * hwo2014bot
 * hwo2014
 */

package hwo2014

/**
 * Car
 * @param id identification of the car
 * @param dimensions dimensions of the car
 */
case class Car(id: CarId, dimensions: Dimension)

case class CarInfo(id: CarId, speed:Double, index:Int, inPieceDistance:Double, laneIndex:Int)

/**
 * CarId
 * @param name name of the bot/car
 * @param color color of the bot/car
 */
case class CarId(name: String, color: String) {
  override def toString: String = {
    s"""CarId{name=$name, color=$color}\n"""
  }
}

/**
 * Dimension
 * @param length length of the car (i.e. 40.0)
 * @param width width of the car (i.e. 20.0)
 * @param guideFlagPosition position of the guiding flag on the car (i.e. 10.0)
 */
case class Dimension(length: Double, width: Double, guideFlagPosition: Double)

/**
 * List of car positions
 * @param positions positions of each car
 */
case class CarPositions(positions: List[CarPosition])

/**
 * Car Position
 * @param id identification of the car
 * @param angle angle of the car
 * @param piecePosition position of the car in current piece
 */
case class CarPosition(id: CarId, angle: Double, piecePosition: PiecePosition)

/**
 * Piece Position
 * @param pieceIndex index of the piece in track
 * @param inPieceDistance distance in the piece
 * @param lane lane position
 * @param lap lap number
 */
case class PiecePosition(pieceIndex: Int, inPieceDistance: Double, lane: LanePosition, lap: Int)

/**
 * Lane Position (value differs if piece is a switch?)
 * @param startLaneIndex lane index at the start of the piece
 * @param endLaneIndex lane index at the end of the piece
 */
case class LanePosition(startLaneIndex: Int, endLaneIndex: Int)
